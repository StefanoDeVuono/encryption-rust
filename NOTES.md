## RSA

### messages

message = “Ssh. This is a secret”
message = 5
message = 7
message = an Integer

### ciphers

cipher = “Tti/!Uijt!jt!b!tfdsfu”
cipher = 6
cipher = 8
cipher = an Integer + 1

### decryption

Decrypt the cipher by subtracting 1
cipher = message + 1
message = cipher - 1
message = (message + 1) - 1

### Caesar

This is a Caesar cipher. It’s not very secure
What we want is a function that is hard to reverse
