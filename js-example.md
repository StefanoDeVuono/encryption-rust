### JavaScript example

```javascript
p = 2053n // or pick your own prime!
q = 7879n // or pick another prime!
phiP = 2052n
phiQ = 7878n
n = p * q

gcd = (a,b) => {
     if (a === b) return a
     if (a>b) return gcd(a-b, b)
     return gcd(a, b-a)
 }

c = gcd(phiP, phiQ)
lambda = phiP * phiQ / c

modInverse = (a, m) => {
     a = a %m
     for (let i = BigInt(1); i < m; i++) if ((a*i) % m === BigInt(1)) return i
 }

e = 65537n
d = modInverse(e, lambda) // 274661n

message = 3289043n
cipher = (message ** e) % n
result = (cipher ** d) % n

result === message
```