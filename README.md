### NOTE: This implementation is not cryptographically secure. It's just for reference!

Three steps:

1. Find two large primes \$`p`$ and $`q`\$.
1. Compute \$`n = pq`$ and $`k = \phi(n)`$ or $`k = \lambda(n)`\$.
   - \$`\phi`$ is Euler's totient. $`\phi(n)`\$ is the number of integers from 1 to n for which the greatest common divisor is 1.
   - The number \$`9`$, for example, has 6 totatives $`\{1,\ 2,\ 4,\ 5,\ 7,\ 8\}`$. $`3`$, and  $`6`$, share with  $`9`$, the greatest common divisor of $`3`$. $`\phi(9) = 6`\$.
   - The totient of a prime \$`p`$_ is always $`p-1`\$\_, since all positive integers less than it are relatively prime.
   - The totient of a product is the product of the totients: \$`\phi(mn) = \phi(m) \cdot \phi(n)`$ (in our case, $`(p-1) \cdot \(q - 1)`\$)
   - We can use carmichael which is often a smaller number. The carmichael of a number \$`n`$ (defined as $`\lambda(n)`$) is the smallest number $`m`$ that any of $`n`$'s totatives a will work in the following formula $`a^{\lambda(n) \equiv 1 \pmod n}`$. The carmichael of the product of two primes $`\lambda(pq)`$ is the $`lcm((p-1)(q-1))`$ or $`\frac{\phi(pq)}{gcd(p-1, q-1)}`\$.
1. Find two numbers \$`e`$  and $`d`$  such that $`ed \equiv 1 \pmod k`\$ .
   - what does this mean?
   - it means that \$`ed - 1 = ki`$ where $`i`\$ is an integer.
   - We often start with an \$`e =65537`$. Although, we have to pick a different prime $`e`$ if $`e`$ is a factor of $`k`\$.
1. If we have a message \\$`m`\$ smaller than our \$`n`\$ (the product of \$`p \cdot q`\$ from above), we can encrypt it with \$`c = m^e \mod n`\$ and decrypt the result with \$`m = c^d \mod n`\$
